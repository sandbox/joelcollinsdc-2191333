<?php

/**
 * @file
 * Functions needed to execute image elements provided by Image module.
 */

/**
 * Implements hook_widgets_element_info().
 */
function socialmedia_widgets_element_info() {
  $facebook_js_sdk = <<<EOF
<script>if(window.FB){ window.FB = null ;}//eat this !</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=223324211035941";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
EOF;
  //$addthis_js = '<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=[?addthis_username:[socialmedia:addthis:username]]"></script>';
  $addthis_js = '<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=tomdude48"></script>';
  
  $elements = array(
    'socialmedia_twiter-account-text-link' => array(
      'label' => t('Socialmedia Twitter account text link'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Twitter'),
      'template' => '<a href="[?twitter_url=[socialmedia:sm-twitter_url]?]">[?twitter_username=[socialmedia:sm-twitter_amp-username]?]</a>',
      'form callback' => 'widgets_template_auto_form',
    ),
    'socialmedia_twitter-account-link' => array(
      'label' => t('Socialmedia Twitter account button'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Twitter'),
      'template' => '<a href="[?twitter_url=[socialmedia:twitter_url]?]"><img src="[socialmedia:twitter_icon]" alt="[?twitter_amp-username=[socialmedia:twitter_amp-username]?]" /></a>',
      'form callback' => 'widgets_template_auto_form',
    ), 
    'socialmedia_twitter-tweet-no-count' => array(
      'label' => t('Twitter tweet button (no count)'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Twitter'),
      'template' => '<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>',
      //'form callback' => 'widgets_template_auto_form',
    ), 
    'socialmedia_twitter-tweet-horizontal-count' => array(
      'label' => t('Twitter tweet button with horizontal count'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Twitter'),
      'template' => '<a href="https://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>',
          //'form callback' => 'widgets_template_auto_form',
    ), 
// Facebook widgets
    'socialmedia_facebook-like-box' => array(
      'label' => t('Facebook like box'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Facebook'),
      'template' => '<div class="fb-like-box" data-href="[?href=[socialmedia:sm-facebook_url]?]" data-width="292" data-show-faces="true" data-stream="true" data-header="true"></div>',
      'add_js' => array(
        'data' => $facebook_js_sdk,
        'options' => array(
          'scope' => 'set_pre', 
          'type' => 'inline'
        ),
      ),
      'form callback' => 'widgets_template_auto_form',
    ),
// AddThis widgets
    'socialmedia_addthis-tweet' => array(
      'label' => t('Addthis tweet button'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Twitter'),
      'template' => '<a class="addthis_button_tweet"[?count=?]tw:related="tutrtv:Tutr.tv" tw:via="[?via=[socialmedia:sm-twitter_username]?]"></a>',
      'add_js' => array(
        'data' => $addthis_js,
        'options' => array(
          'scope' => 'set_post', 
        ),
      ),
      'form callback' => 'socialmedia_widgets_addthis_tweet_form',
    ),
    'socialmedia_addthis-facebook-like' => array(
      'label' => t('Addthis Facebook like button'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Facebook'),
      'template' => '<a class="addthis_button_facebook_like"[?layout=?] fb:like:width="450" fb:like:show_faces="true"></a>',
      'add_js' => array(
        'data' => $addthis_js,
        'options' => array(
          'scope' => 'set_post', 
        ),
      ),
      'form callback' => 'socialmedia_widgets_addthis_facebook_like_form',
    ),
    'socialmedia_addthis-facebook-send' => array(
      'label' => t('Addthis Facebook send button'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Facebook'),
      'template' => '<a class="addthis_button_facebook_like"[?layout=?]></a>',
      'add_js' => array(
        'data' => $addthis_js,
        'options' => array(
          'scope' => 'set_post', 
        ),
      ),
      'form callback' => 'socialmedia_widgets_addthis_facebook_send_form',
    ),
    'socialmedia_addthis-google-plusone' => array(
      'label' => t('Addthis Google +1 button'),
      'help' => t('Resizing will make images an exact set of dimensions. This may cause images to be stretched or shrunk disproportionately.'),
      'group' => t('Social media: Google+'),
      'template' => '<a class="addthis_button_google_plusone"[?size=?]></a>',
      'add_js' => array(
        'data' => $addthis_js,
        'options' => array(
          'scope' => 'set_post', 
        ),
      ),
      'form callback' => 'socialmedia_widgets_addthis_google_plusone_form',
    ),
  );

  return $elements;
}

function socialmedia_widgets_addthis_tweet_form($data) {
  $options = array(
    '' => t('None'),
    ' tw:count="vertical"' => t('Vertical'),
    ' tw:count="horizontal"' => t('Horizontal'),
  );
  $form['count'] = array(
    '#type' => 'select',
    '#title' => t('Count'),
    '#default_value' => (isset($data['count']) && $data['count']) ? $data['count'] : '',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#options' => $options,
  );
  $form['via'] = array(
    '#type' => 'textfield',
    '#title' => t('Via'),
    '#default_value' => (isset($data['via']) && $data['via']) ? $data['via'] : '',
    '#field_prefix' => '@',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
  );
  return $form;
}

function socialmedia_widgets_addthis_facebook_like_form($data) {
	return socialmedia_widgets_addthis_facebook_form($data, 'like');
}

function socialmedia_widgets_addthis_facebook_send_form($data) {
  return socialmedia_widgets_addthis_facebook_form($data, 'send');
}

function socialmedia_widgets_addthis_facebook_form($data, $type) {
  $options = array(
    '' => t('Standard'),
    ' fb:' . $type . ':layout="button_count"' => t('Button count'),
    ' fb:' . $type . ':layout="box_count"' => t('Box count'),
  );
  $form['layout'] = array(
    '#type' => 'select',
    '#title' => t('Layout'),
    '#default_value' => (isset($data['layout']) && $data['layout']) ? $data['layout'] : '',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#options' => $options,
  );
  /*
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => (isset($data['width']) && $data['width']) ? $data['width'] : '',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
  );

  $form['show_faces'] = array(
    '#type' => 'checkbox',
    '#title' => t('Faces'),
    '#return_value' => ' fb:' . $type . ':show_faces="true"',
    '#default_value' => (isset($data['show_faces']) && $data['show_faces']) ? $data['show_faces'] : '',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
  );
  */
  return $form;
}

function socialmedia_widgets_addthis_google_plusone_form($data) {
  $options = array(
    ' g:plusone:size="standard"' => t('Standard'),
    ' g:plusone:size="small"' => t('Small'),
    ' g:plusone:size="medium"' => t('Medium'),
    ' g:plusone:size="tall"' => t('Tall'),
  );
  $form['size'] = array(
    '#type' => 'select',
    '#title' => t('Size'),
    '#default_value' => (isset($data['size']) && $data['size']) ? $data['size'] : '',
    //'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
    '#options' => $options,
  );
  return $form;
}



function socialmedia_widgets_default_sets() {
  $sets = array();

  $sets['socialmedia_text-signature'] = array(
    'elements' => array(
      array(
        'name' => 'widgets_custom-markup',
        'data' => array('markup' => 'Follow us on:'),
        'weight' => 0,
      ),
      array(
        'name' => 'socialmedia_twiter-account-text-link',
        'data' => array(),
        'weight' => 1,
      ),
    ),
  );

  return $sets;
}